using System;
using MonoTouch.UIKit;
using System.Drawing;
using BD.Bluetooth.Connection;

namespace BluetoothDemo
{
	public class TableViewSource : UITableViewSource
	{
		BTDeviceManager _devicesManager;

		public TableViewSource(BTDeviceManager devicesManager)
		{
			_devicesManager = devicesManager;
		}

		public override int NumberOfSections (UITableView tableView)
		{
			return 1;
		}

		public override int RowsInSection (UITableView tableview, int section)
		{
			return _devicesManager.SortedDevices.Count;
		}

		public override UITableViewCell GetCell (UITableView tableView, MonoTouch.Foundation.NSIndexPath indexPath)
		{
			RectangleF frame = new RectangleF (0f, 0f, tableView.Frame.Width, tableView.RowHeight);
			UITableViewCell cell = new UITableViewCell(UITableViewCellStyle.Value1, null);
			cell.Frame = frame;

			//cell.TextLabel.Text = String.Format("Dit is cell {0}", _i);
			cell.TextLabel.Text = _devicesManager.SortedDevices[indexPath.Row].DeviceName;
			cell.DetailTextLabel.Text = _devicesManager.SortedDevices[indexPath.Row].Description;

			return cell;
		}
	}
}

