using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using BD.Bluetooth.Connection;
using BD.Bluetooth.Core;
using BD.Bluetooth.Providers;

namespace BluetoothDemo
{
	public partial class BluetoothDemoViewController : UIViewController
	{
		private UITableView tableView;
		BTDeviceManager _devicesManager;
		BTDataHandler _dataHandler;
		BTSessionManager _sessionManager;


		public BluetoothDemoViewController () : base()
		{
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			
			// Perform any additional setup after loading the view, typically from a nib.

			tableView = new UITableView(View.Bounds);
			tableView.AutoresizingMask = UIViewAutoresizing.FlexibleDimensions;
			tableView.BackgroundColor = UIColor.LightGray;
			View.Add(tableView);

			_devicesManager = new BTDeviceManager ();
			_dataHandler = new BTDataHandler (CreateSpecificDataProvider(), _devicesManager);
			_sessionManager = new BTSessionManager (_dataHandler, _devicesManager);

			tableView.Source = new TableViewSource(_devicesManager);

			// Notifications being called from the SessionManager when devices become available/unavailable
			InvokeInBackground(delegate {
				NSNotificationCenter.DefaultCenter.AddObserver(this, new MonoTouch.ObjCRuntime.Selector ("DeviceAvailable"), BTConstants.BD_BLUETOOTH_NOTIFICATION_DEVICE_AVAILABLE, null);
				NSNotificationCenter.DefaultCenter.AddObserver(this, new MonoTouch.ObjCRuntime.Selector ("DeviceUnavailable"), BTConstants.BD_BLUETOOTH_NOTIFICATION_DEVICE_UNAVAILABLE, null);

				_sessionManager.start();
			});
		}

		IBTDataProvider CreateSpecificDataProvider() {
			return new BTImageDataProvider (this);
		}

		[Export("DeviceAvailable")]
		void DeviceAvailable(NSNotification notification) {
			InvokeOnMainThread (delegate {
				Console.WriteLine("DeviceAvailable");
				tableView.ReloadData();
			});
		}

		[Export("DeviceUnavailable")]
		void DeviceUnavailable(NSNotification notification) {
			InvokeOnMainThread (delegate {
				Console.WriteLine("DeviceUnavailable");
				tableView.ReloadData();
			});
		}

		public override bool ShouldAutorotateToInterfaceOrientation (UIInterfaceOrientation toInterfaceOrientation)
		{
			// Return true for supported orientations
			return true;
		}
	}
}

