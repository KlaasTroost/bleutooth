// WARNING
//
// This file has been generated automatically by Xamarin Studio to store outlets and
// actions made in the Xcode designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using MonoTouch.Foundation;

namespace BD.Bluetooth
{
	[Register ("BluetoothViewController")]
	partial class BluetoothViewController
	{
		[Outlet]
		MonoTouch.UIKit.UITableView devicesTable { get; set; }

		[Outlet]
		MonoTouch.UIKit.UILabel deviceNameLabel { get; set; }

		[Outlet]
		MonoTouch.UIKit.UIImageView imageView { get; set; }
		
		void ReleaseDesignerOutlets ()
		{
			if (devicesTable != null) {
				devicesTable.Dispose ();
				devicesTable = null;
			}

			if (deviceNameLabel != null) {
				deviceNameLabel.Dispose ();
				deviceNameLabel = null;
			}

			if (imageView != null) {
				imageView.Dispose ();
				imageView = null;
			}
		}
	}
}
