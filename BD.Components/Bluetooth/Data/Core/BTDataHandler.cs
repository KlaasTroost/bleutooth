/**
 * Protocol voor het ontvangen en verzenden van berichtjes via Bluetooth. Het gebruikt de doorgegeven dataprovider
 * voor het verzenden en ontvangen/bewaren van de data.
 * Nu volgt een voorbeeld van het gebruik van het protocol (Eerste 4 bytes bevat het commando de rest de data).
 * 
 * 			Peer A -> SENDFoto gemaakt om 11:15
 * 			Peer B -> ACPT
 * 			Peer A -> SIZE213312
 * 			Peer B -> ACKN
 * 			Peer A -> <foto data>
 * 			Peer B -> SUCS
 * 
 * Wil je een specifieke dataprovider maken dan moet je erven van IBTDataProvider. 
 * Onderstaande dataproviders zijn al gerealiseerd:
 * - BTImagaDataProvider
 * - BTStringDataProvider
 **/

using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.GameKit;
using System.Drawing;
using MonoTouch.AudioToolbox;
using System.Text;
using BD.Bluetooth.Connection;

namespace BD.Bluetooth.Core
{
	public enum BTDataHandlerState
	{
		DHSNone,
		DHSReceiving,
		DHSSending
	}

	public class BTDataHandler : UIAlertViewDelegate
	{
		public const string BD_BTDATAHANDLER_REQUESTING_PERMISSION_TO_SEND = @"SEND";
		public const string BD_BTDATAHANDLER_ACCEPT_CONTACT = @"ACPT";
		public const string BD_BTDATAHANDLER_REJECT_CONTACT = @"RJCT";
		public const string BD_BTDATAHANDLER_INFO_SIZE = @"SIZE";
		public const string BD_BTDATAHANDLER_ACKNOWLEDGE = @"ACKN";
		public const string BD_BTDATAHANDLER_SUCCESS = @"SUCS";
		public const string BD_BTDATAHANDLER_I_AM_BUSY = @"BUSY";
		public const string BD_BTDATAHANDLER_ERROR = @"ERRO";
		public const string BD_BTDATAHANDLER_CANCEL = @"CNCL";

		public const int BD_BTDATAHANDLER_PROCESSING_TAG = 0;
		public const int BD_BTDATAHANDLER_CONFIRMATION_RETRY_TAG = 1;
		public const int BD_BTDATAHANDLER_CONFIRMATION_RECEIVE_TAG = 2;

		const string BD_BT_AVAILABLE_SOUND_FILE_NAME = "Sounds/available.aiff";
		const string BD_BT_UNAVAILABLE_SOUND_FILE_NAME = "Sounds/unavailable.aiff";
		const string BD_BT_ERROR_SOUND_FILE_NAME = "Sounds/error.aiff";
		const string BD_BT_RECEIVED_SOUND_FILE_NAME = "Sounds/received.aiff";
		const string BD_BT_REQUEST_SOUND_FILE_NAME = "Sounds/request.aiff";
		const string BD_BT_SEND_SOUND_FILE_NAME = "Sounds/sent.aiff";

		BTDataHandlerState _currentState;
		BTDevice _currentStateRelatedDevice;
		string _lastCommandReceived;

		BTDeviceManager _devicesManager;

		UIAlertView _currentPopUpView;

		int _bytesToReceive;

		IBTDataProvider _dataProvider;

		SystemSound _availableSound;
		SystemSound _unavailableSound;
		SystemSound _errorSound;
		SystemSound _receivedSound;
		SystemSound _requestSound;
		SystemSound _sendSound;

		public BTDataHandler (IBTDataProvider provider, BTDeviceManager manager)
		{
			_currentState = BTDataHandlerState.DHSNone;
			_dataProvider = provider;
			_devicesManager = manager;

			LoadSounds ();

			NSNotificationCenter.DefaultCenter.AddObserver (this, new MonoTouch.ObjCRuntime.Selector ("BTDataHandler.DeviceAvailable"), BTConstants.BD_BLUETOOTH_NOTIFICATION_DEVICE_AVAILABLE, null);
			NSNotificationCenter.DefaultCenter.AddObserver (this, new MonoTouch.ObjCRuntime.Selector ("BTDataHandler.DeviceUnavailable"), BTConstants.BD_BLUETOOTH_NOTIFICATION_DEVICE_UNAVAILABLE, null);
		}
		void LoadSounds() {
			_availableSound = SystemSound.FromFile (BD_BT_AVAILABLE_SOUND_FILE_NAME);
			_unavailableSound = SystemSound.FromFile (BD_BT_UNAVAILABLE_SOUND_FILE_NAME);
			_errorSound = SystemSound.FromFile (BD_BT_ERROR_SOUND_FILE_NAME);
			_receivedSound = SystemSound.FromFile (BD_BT_RECEIVED_SOUND_FILE_NAME);
			_requestSound = SystemSound.FromFile (BD_BT_REQUEST_SOUND_FILE_NAME);
			_sendSound = SystemSound.FromFile (BD_BT_SEND_SOUND_FILE_NAME);
		}
		[Export ("BTDataHandler.DeviceAvailable")]
		public void DeviceAvailable() {
			_availableSound.PlaySystemSound ();
		}
		[Export ("BTDataHandler.DeviceUnavailable")]
		public void DeviceUnavailable() {
			_unavailableSound.PlaySystemSound ();
		}
		public void SendToDevice(BTDevice device) {
#if DEBUG
			Console.WriteLine ("DataHandler.SendToDevice(Device: {0})", device.PeerID);
#endif
			// Called from the main ViewController when someone selects a device on the table

			// Sets the DataHandler to occupied
			_currentState = BTDataHandlerState.DHSSending;
			_currentStateRelatedDevice = device;

			// When the Provider gets the data to send properly (eg. a contact), the dataPrepared method will be called
			_dataProvider.PrepareDataAndReplyTo (this, new MonoTouch.ObjCRuntime.Selector("DataPrepared"));
		}

		public void ReceiveData(NSData data, string fromPeer, GKSession inSession, NSObject context) {
#if DEBUG
			Console.WriteLine ("DataHandler.ReceiveData(fromPeer: {0}) - _currentState: {1}", fromPeer, _currentState);
#endif
			// Caller whenever data is received from the session

			BTDevice device = _devicesManager.DeviceWithID(fromPeer);

			if (device != null) {
				// Checks if it's busy, otherwise call other handler methods
				switch (_currentState) {
				case BTDataHandlerState.DHSNone:
					_currentState = BTDataHandlerState.DHSReceiving;
					_currentStateRelatedDevice = device;

					HandleReceivingData(data);
					break;
				case BTDataHandlerState.DHSReceiving:
					if (!_currentStateRelatedDevice.Equals(device) || GetCommandFromMessage(data).Equals(BD_BTDATAHANDLER_REQUESTING_PERMISSION_TO_SEND)) {
						SendBusyData(device);
					} else {
						HandleReceivingData(data);
					}
					break;
				case BTDataHandlerState.DHSSending:
					if (!_currentStateRelatedDevice.Equals(device) || GetCommandFromMessage(data).Equals(BD_BTDATAHANDLER_REQUESTING_PERMISSION_TO_SEND)) {
						SendBusyData(device);
					} else {
						HandleSendingData(data);
					}
					break;
				default:
					break;
				}
			}
		}

		string GetCommandFromMessage(NSData message) {
			// The 4 first bytes of the message represent the command
#if DEBUG
			Console.WriteLine ("DataHandler.GetCommandFromMessage(NSData message)");
#endif
			string strMsg = "";
			if (message != null && message.Length >= 4) {
				string msg = message.ToString ();
				if (msg != null) {
					strMsg = msg.Substring (0, 4);
				}
			}
			return strMsg;

		}

		byte[] GetValueFromMessage(NSData message) {
			// All the data after the first 4 bytes are considered the value
#if DEBUG
			Console.WriteLine ("DataHandler.GetValueFromMessage(NSData message)");
#endif
			if (message != null && message.Length >= 4 && GetCommandFromMessage(message).Length > 0) {
				byte[] baits = new byte[message.Length - 4];
				for (int x = 4; x < message.Length; x++) {
					baits [x - 4] = message [x];
				}
				return baits;
			} else {
				byte[] returnData = new byte[message.Length];
				System.Runtime.InteropServices.Marshal.Copy (
					message.Bytes,
					returnData,
					0,
					Convert.ToInt32 (message.Length)
					);

				return returnData;
			}
		}

		void UpdateLastCommandReceived(string command) {
#if DEBUG
			Console.WriteLine ("DataHandler.UpdateLastCommandReceived(command: {0})", command);
#endif
			if (_lastCommandReceived != null)
				_lastCommandReceived = null;
			_lastCommandReceived = command;
		}

		void HandleReceivingData(NSData data) {
			string command = GetCommandFromMessage(data);
#if DEBUG
			Console.WriteLine ("DataHandler.HandleReceivingData(NSData data) - command: {0}", command);
#endif

			// First, check for specific error situations
			if (command.Equals(BD_BTDATAHANDLER_ERROR)) {
				ThrowError(string.Format(NSBundle.MainBundle.LocalizedString(@"BD_BT_RECEIVED_ERROR_ERROR", @"Received an error message"), _currentStateRelatedDevice.DeviceName));
			} else if (command.Equals(BD_BTDATAHANDLER_CANCEL)) {
				ThrowError(string.Format(NSBundle.MainBundle.LocalizedString(@"BD_BT_PEER_CANCELLED_ERROR", @"Transfer cancelled"), _currentStateRelatedDevice.DeviceName));
			} else if (command.Equals(BD_BTDATAHANDLER_I_AM_BUSY)) {
				ThrowError(string.Format(NSBundle.MainBundle.LocalizedString(@"BD_BT_RECEIVED_BUSY_ERROR", @"Receiver is busy"), _currentStateRelatedDevice.DeviceName));
			} else {
				// If it's not an error, then let's check the command and compare it to the last command received to check if the command is expected
				if (_lastCommandReceived == null) {
					if (!command.Equals(BD_BTDATAHANDLER_REQUESTING_PERMISSION_TO_SEND)) {
						SendErrorData();
						ThrowUnexpectedCommandError();
					} else {
						// Prompt the user whether to receive the contact or not
						_requestSound.PlaySystemSound ();
						PromptConfirmationWithTag(
							BD_BTDATAHANDLER_CONFIRMATION_RECEIVE_TAG,
							NSBundle.MainBundle.LocalizedString(@"BD_BT_RECEIVE_VIEW_TITLE", @"Dialog title when receiving data"), 
							string.Format(NSBundle.MainBundle.LocalizedString(@"BD_BT_RECEIVE_VIEW_PROMPT", @"Dialog text when receiving data"), _currentStateRelatedDevice.DeviceName));
						UpdateLastCommandReceived(command);
					}
				} else if (_lastCommandReceived.Equals(BD_BTDATAHANDLER_REQUESTING_PERMISSION_TO_SEND)) {
					if (!command.Equals(BD_BTDATAHANDLER_INFO_SIZE)) {
						SendErrorData();
						ThrowUnexpectedCommandError();
					} else {
						_bytesToReceive = int.Parse(Encoding.UTF8.GetString( GetValueFromMessage(data)));
						SendAcknowledgeData();

						UpdateLastCommandReceived(command);
					}
				} else if (_lastCommandReceived.Equals(BD_BTDATAHANDLER_INFO_SIZE)) {
					// Check whether the data has the expected size
					if (_bytesToReceive == data.Length) {
						// Receive the real data (eg. contact) and tell the provider to store it
						bool dataCanBeStored = _dataProvider.StoreData (data, this, new MonoTouch.ObjCRuntime.Selector("DataStored"));	// andReplyTo:self selector:@selector(dataStored)];

						if (dataCanBeStored) {
							SendSuccessData();
							CloseCurrentPopup();
							_receivedSound.PlaySystemSound ();
						} else {
							SendErrorData();
							ThrowError(string.Format(NSBundle.MainBundle.LocalizedString(@"BD_BT_RECEPTION_ERROR", @"Error receiving data"), _currentStateRelatedDevice.DeviceName));
						}

					} else {
						SendErrorData();
						ThrowError(string.Format(NSBundle.MainBundle.LocalizedString(@"BD_BT_RECEPTION_ERROR ",@"Error receiving data"), _currentStateRelatedDevice.DeviceName));
					}
				} else {
					SendErrorData();
					ThrowUnexpectedCommandError();
				}
			}
		}

		[Export ("DataStored")]
		public void DataStored() {
#if DEBUG
			Console.WriteLine ("DataHandler.DataStored()");
#endif
			CleanCurrentState();
		}

		void HandleSendingData(NSData data) {
			string command = GetCommandFromMessage(data);
#if DEBUG
			Console.WriteLine ("DataHandler.HandleSendingData(NSData data) - command: {0}", command);
#endif

			// First, check for specific error situations
			if (command.Equals(BD_BTDATAHANDLER_ERROR)) {
				ThrowError(string.Format(NSBundle.MainBundle.LocalizedString(@"BD_BT_RECEIVED_ERROR_ERROR", @"Received an error message"), 
				                  _currentStateRelatedDevice.DeviceName));
			} else if (command.Equals(BD_BTDATAHANDLER_CANCEL)) {
				ThrowError(string.Format(NSBundle.MainBundle.LocalizedString(@"BD_BT_PEER_CANCELLED_ERROR", @"Transfer cancelled"), 
				                  _currentStateRelatedDevice.DeviceName));
			} else if (command.Equals(BD_BTDATAHANDLER_I_AM_BUSY)) {
				ThrowError(string.Format(NSBundle.MainBundle.LocalizedString(@"BD_BT_RECEIVED_BUSY_ERROR", @"Receiver is busy"), 
				                  _currentStateRelatedDevice.DeviceName));
			} else {
				// If it's not an error, then let's check the command and compare it to the last command received to check if the command is expected
				if (_lastCommandReceived == null) {
					if (command.Equals(BD_BTDATAHANDLER_ACCEPT_CONTACT)) {
						SendSizeData();

						UpdateLastCommandReceived(command);
					} else if (command.Equals(BD_BTDATAHANDLER_REJECT_CONTACT)) {
						// Prompt the user whether to retry to send or not
						PromptConfirmationWithTag(BD_BTDATAHANDLER_CONFIRMATION_RETRY_TAG ,
						                          NSBundle.MainBundle.LocalizedString(@"BD_BT_RETRY_VIEW_TITLE", @"Transfer refused dialog title"),
						                          string.Format(NSBundle.MainBundle.LocalizedString(@"BD_BT_RETRY_VIEW_PROMPT", @"Transfer refused dialog text"),
						    _currentStateRelatedDevice.DeviceName, _dataProvider.GetLabelOfDataToSend ()));
					} else {
						SendErrorData();
						ThrowUnexpectedCommandError();
					}
				} else if (_lastCommandReceived.Equals(BD_BTDATAHANDLER_ACCEPT_CONTACT)) {
					if (!command.Equals(BD_BTDATAHANDLER_ACKNOWLEDGE)) {
						SendErrorData();
						ThrowUnexpectedCommandError();
					} else {
						SendRealData();

						UpdateLastCommandReceived(command);
					}
				} else if (_lastCommandReceived.Equals(BD_BTDATAHANDLER_ACKNOWLEDGE)) {
					if (!command.Equals(BD_BTDATAHANDLER_SUCCESS)) {
						SendErrorData();
						ThrowUnexpectedCommandError();
					} else {
						_sendSound.PlaySystemSound ();
						ShowMessageWithTitle(
							NSBundle.MainBundle.LocalizedString(@"BD_BT_SUCCESS_VIEW_TITLE", @"Transfer completed dialog title."),
							string.Format(NSBundle.MainBundle.LocalizedString(@"BD_BT_SEND_SUCCESS_MESSAGE", @"Transfer completed dialog text."),
						         _dataProvider.GetLabelOfDataToSend (), _currentStateRelatedDevice.DeviceName));
						CleanCurrentState();
					}
				} else {
					SendErrorData();
					ThrowUnexpectedCommandError();
				}
			}	
		}

		public override void Clicked (UIAlertView alertView, int buttonIndex)
		{
#if DEBUG
			Console.WriteLine ("DataHandler.Clicked(alertView, buttonIndex: {0})", buttonIndex);
#endif
			if (alertView.Tag == BD_BTDATAHANDLER_CONFIRMATION_RECEIVE_TAG) {
				if (buttonIndex == 1) { // YES
					CloseCurrentPopup();
					SendAcceptData();
				} else { // NO
					CloseCurrentPopup();
					SendRejectData();
					CleanCurrentState();
				}
			} else if (alertView.Tag == BD_BTDATAHANDLER_CONFIRMATION_RETRY_TAG) {
				if (buttonIndex == 1) { // YES
					CloseCurrentPopup();
					SendRequestData();
				} else { // NO
					CleanCurrentState();
				}
			} else if (alertView.Tag == BD_BTDATAHANDLER_PROCESSING_TAG) {
				// Clicked on CANCEL

				CloseCurrentPopup();

				if (_currentStateRelatedDevice.IsConnected())
					SendCancelData();

				CleanCurrentState();
			}
		}

		NSData DataFromString(string str) {
//			return [str dataUsingEncoding:NSUTF8StringEncoding];
#if DEBUG
			Console.WriteLine ("DataHandler.DataFromString(str: {0})", str);
#endif
			return NSData.FromString (str);
		}

		void SendBusyData(BTDevice device) {
#if DEBUG
			Console.WriteLine ("DataHandler.SendBusyData(Device: {0})", device.PeerID);
#endif
			NSError error;
			device.SendData (DataFromString(BD_BTDATAHANDLER_I_AM_BUSY), out error);
		}

		void SendCancelData() {
#if DEBUG
			Console.WriteLine ("DataHandler.SendCancelData()");
#endif
			NSError error;
			_currentStateRelatedDevice.SendData(DataFromString(BD_BTDATAHANDLER_CANCEL), out error);
		}

		void SendErrorData() {
#if DEBUG
			Console.WriteLine ("DataHandler.SendErrorData()");
#endif
			NSError error;
			_currentStateRelatedDevice.SendData(DataFromString(BD_BTDATAHANDLER_ERROR), out error);
		}

		void SendRequestData() {
#if DEBUG
			Console.WriteLine ("DataHandler.SendRequestData()");
#endif
			ShowProcess(string.Format(NSBundle.MainBundle.LocalizedString(@"BD_BT_WAITING_FOR_ACCEPTANCE_PROCESS", @"Waiting for acceptance"), 
			                   _currentStateRelatedDevice.DeviceName));
			string strToSend = string.Format(@"{0}{1}", BD_BTDATAHANDLER_REQUESTING_PERMISSION_TO_SEND, _dataProvider.GetLabelOfDataToSend());
			NSError error;
			_currentStateRelatedDevice.SendData(DataFromString(strToSend), out error);
		}

		void SendSizeData() {
#if DEBUG
			Console.WriteLine ("DataHandler.SendSizeData()");
#endif
			ShowProcess(NSBundle.MainBundle.LocalizedString(@"BD_BT_SENDING_PROCESS", @"Sending data dialog"));
			string strToSend = string.Format(@"{0}{1}", BD_BTDATAHANDLER_INFO_SIZE, _dataProvider.GetDataToSend().Length);
			NSError error;
			_currentStateRelatedDevice.SendData(DataFromString(strToSend), out error);
		}

		void SendAcceptData() {
#if DEBUG
			Console.WriteLine ("DataHandler.SendAcceptData()");
#endif
			NSError error;
			_currentStateRelatedDevice.SendData(DataFromString(BD_BTDATAHANDLER_ACCEPT_CONTACT), out error);
		}

		void SendRejectData() {
#if DEBUG
			Console.WriteLine ("DataHandler.SendRejectData()");
#endif
			NSError error;
			_currentStateRelatedDevice.SendData(DataFromString(BD_BTDATAHANDLER_REJECT_CONTACT), out error);
		}

		void SendAcknowledgeData() {
#if DEBUG
			Console.WriteLine ("DataHandler.SendAcknowledgeData()");
#endif
			ShowProcess(NSBundle.MainBundle.LocalizedString(@"BD_BT_RECEIVING_PROCESS", @"Receiving data dialog"));
			NSError error;
			_currentStateRelatedDevice.SendData (DataFromString(BD_BTDATAHANDLER_ACKNOWLEDGE), out error);
		}

		void SendSuccessData() {
#if DEBUG
			Console.WriteLine ("DataHandler.SendSuccessData()");
#endif
			NSError error;
			_currentStateRelatedDevice.SendData(DataFromString(BD_BTDATAHANDLER_SUCCESS), out error);
//			if (error == null || error.Code == 0) {
//				_currentStateRelatedDevice.CancelConnection ();
//			}
		}

		void SendRealData() {
#if DEBUG
			Console.WriteLine ("DataHandler.SendRealData()");
#endif
			ShowProcess(NSBundle.MainBundle.LocalizedString(@"BD_BT_SENDING_PROCESS", @"Sending data dialog"));
			NSError error;
			_currentStateRelatedDevice.SendData(_dataProvider.GetDataToSend(), out error);
		}

		[Export("DataPrepared")]
		public void DataPrepared() {
#if DEBUG
			Console.WriteLine ("DataHandler.DataPrepared()");
#endif
			ShowProcess(NSBundle.MainBundle.LocalizedString(@"BD_BT_CONNECTION_PROCESS", @"Connecting dialog"));

			if (!_currentStateRelatedDevice.IsConnected ()) {
				_currentStateRelatedDevice.ConnectAndReplyTo (this, new MonoTouch.ObjCRuntime.Selector("DeviceConnected"), new MonoTouch.ObjCRuntime.Selector("DeviceConnectionFailed"));
			} else {
				DeviceConnected();
			}
		}

		[Export ("DeviceConnected")]
		public void DeviceConnected() {
#if DEBUG
			Console.WriteLine ("DataHandler.DeviceConnected()");
#endif
			SendRequestData();
		}

		[Export ("DeviceConnectionFailed")]
		public void DeviceConnectionFailed() {
#if DEBUG
			Console.WriteLine ("DataHandler.DeviceConnectionFailed()");
#endif
			string deviceName = _currentStateRelatedDevice == null ? "unknown" : _currentStateRelatedDevice.DeviceName;
			ThrowError(string.Format(NSBundle.MainBundle.LocalizedString(@"BD_BT_CONNECTION_ERROR", "Error when connecting to peer", deviceName)));
		}

		void ThrowUnexpectedCommandError() {
#if DEBUG
			Console.WriteLine ("DataHandler.ThrowUnexpectedCommandError()");
#endif
			string cmd = _lastCommandReceived ?? "unknown";
			ThrowError(string.Format(NSBundle.MainBundle.LocalizedString(@"BD_BT_UNEXPECTED_COMMAND_ERROR", @"Received unexpected command", cmd)));
		}

		void ShowMessageWithTitle(string title, string msg ){
#if DEBUG
			Console.WriteLine ("DataHandler.ShowMessageWithTitle(title: {0}, msg: {1})", title, msg);
#endif
			CloseCurrentPopup();

			UIAlertView confirmationView = new UIAlertView(title, msg,null, @"OK", null);

			confirmationView.Show();
		}

		void ThrowError(string message) {
#if DEBUG
			Console.WriteLine ("DataHandler.ThrowError(message: {0})", message);
#endif
			_errorSound.PlaySystemSound ();
			ShowMessageWithTitle(@"Error dialog title", message);
			CleanCurrentState();
		}

		void CleanCurrentState() {
#if DEBUG
			Console.WriteLine ("DataHandler.CleanCurrentState()");
#endif
			_currentState = BTDataHandlerState.DHSNone;

			if (_currentStateRelatedDevice != null) {
				_currentStateRelatedDevice = null;
			}

			if (_lastCommandReceived != null) {
				_lastCommandReceived = null;
			}

			_bytesToReceive = 0;

			CloseCurrentPopup();
		}

		void CloseCurrentPopup() {
#if DEBUG
			Console.WriteLine ("DataHandler.CloseCurrentPopup()");
#endif
			if (_currentPopUpView != null) {
				_currentPopUpView.Delegate = null;
				_currentPopUpView.DismissWithClickedButtonIndex (0, true);
				_currentPopUpView.Dispose ();
				_currentPopUpView = null;
			}
		}

		void PromptConfirmationWithTag(int tag, string title, string msg) {
#if DEBUG
			Console.WriteLine ("DataHandler.PromptConfirmationWithTag(tag: {0}, title: {1}, msg: {2})", tag, title, msg);
#endif
			CloseCurrentPopup();

			_currentPopUpView = new UIAlertView (
				title,
				msg,
				this,
				@"No",
				new string[] { "Yes" });
			_currentPopUpView.Tag = tag;

			_currentPopUpView.Show();
		}

		void ShowProcess(string message ){
#if DEBUG
			Console.WriteLine ("DataHandler.ShowProcess(message: {0})", message);
#endif
			CloseCurrentPopup();

			_currentPopUpView = new UIAlertView(message,
			                                    string.Format(@"{0}{0}", Environment.NewLine),
			                    				this,
			                    				@"Cancel",
			                    				null);

			_currentPopUpView.Tag = BD_BTDATAHANDLER_PROCESSING_TAG;

			UIActivityIndicatorView activityView = new UIActivityIndicatorView(new RectangleF(130, 60, 20, 20));
			activityView.StartAnimating ();
			_currentPopUpView.AddSubview(activityView);
			activityView.Dispose();

			_currentPopUpView.Show();
		}
	}
}

