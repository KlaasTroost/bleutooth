using System;
using MonoTouch.Foundation;
using MonoTouch.ObjCRuntime;

namespace BD.Bluetooth.Core
{
	public interface IBTDataProvider
	{
		void PrepareDataAndReplyTo(NSObject dellegate, Selector dataPreparedSelector);
		NSString GetLabelOfDataToSend();
		NSData GetDataToSend();
		bool StoreData(NSData data, NSObject andReplyToDelegate, Selector selector);
		NSDictionary GetDataAsDictionary (NSData data);
	}
}

