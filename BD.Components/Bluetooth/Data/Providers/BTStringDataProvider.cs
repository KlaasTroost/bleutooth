using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using BD.Bluetooth.Core;
using BD.Bluetooth.Connection;

namespace BD.Bluetooth.Providers
{
	public class BTStringDataProvider : UIActionSheetDelegate, IBTDataProvider
	{
		UIViewController _viewController;
		NSObject _delegateToCall;
		MonoTouch.ObjCRuntime.Selector _selectorToCall;

		public BTStringDataProvider (UIViewController viewController)
		{
			_viewController = viewController;
		}

		#region IDataProvider implementation

		public void PrepareDataAndReplyTo (NSObject delegateToCall, MonoTouch.ObjCRuntime.Selector selectorToCall)
		{
			_delegateToCall = delegateToCall;
			_selectorToCall = selectorToCall;

			SendText ();
		}

		public NSString GetLabelOfDataToSend ()
		{
			return new NSString("GetLabelOfDataSend()");
		}

		public NSData GetDataToSend ()
		{
			return NSData.FromString (@"GetDataToSend");
		}

		public bool StoreData (NSData data, NSObject andReplyToDelegate, MonoTouch.ObjCRuntime.Selector selector)
		{
			return true;
		}

		public NSDictionary GetDataAsDictionary (NSData data)
		{
			NSDictionary dictionary = NSDictionary.FromObjectAndKey (data, new NSString(BTConstants.BD_BLUETOOTH_NEW_DATA_KEY));
			return dictionary;
		}

		#endregion

		
		void SendText ()
		{
#if DEBUG
			Console.WriteLine ("DataProvider.SendText()");
#endif
			if (_delegateToCall != null && _selectorToCall != null && _delegateToCall.RespondsToSelector (_selectorToCall)) {
				_delegateToCall.PerformSelector (_selectorToCall, null, 0f);
			}
		}

		#region UIActionSheetDelegate 
		public override void Clicked (UIActionSheet actionSheet, int buttonIndex)
		{
#if DEBUG
			Console.WriteLine ("UIActionSheetDelegate.Clicked(actionSheet, buttonIndex: {0}", buttonIndex);
#endif
			if (buttonIndex == 0) {
				SendText ();
//			} else if (buttonIndex == 1) {
//				SendPicture ();
			}
		}
		public override void Dismissed (UIActionSheet actionSheet, int buttonIndex)
		{
#if DEBUG
			Console.WriteLine ("UIActionSheetDelegate.Dismissed(actionSheet, buttonIndex: {0}", buttonIndex);
#endif
			if (buttonIndex == 0) {
			} else if (buttonIndex == 1) {
			}
		}
		public override void Presented (UIActionSheet actionSheet)
		{
			int aantal = actionSheet.ButtonCount;
#if DEBUG
			Console.WriteLine ("UIActionSheetDelegate.Presented(actionSheet#buttons: {0}", aantal);
#endif
		}
		#endregion
	}
}

