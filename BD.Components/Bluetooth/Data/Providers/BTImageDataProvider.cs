using System;
using MonoTouch.UIKit;
using System.Threading;
using MonoTouch.Foundation;
using System.Text;
using BD.Bluetooth.Core;
using BD.Bluetooth.Connection;

namespace BD.Bluetooth.Providers
{
	public class BTImageDataProvider : IBTDataProvider
	{
		NSData _imageData;
		UIImagePickerController _imagePicker;

		NSObject _delegateToCall;
		MonoTouch.ObjCRuntime.Selector _selectorToCall;

		UIViewController _viewController;

		public BTImageDataProvider (UIViewController viewController)
		{
			_viewController = viewController;
		}

		#region IDataProvider implementation

		public void PrepareDataAndReplyTo (MonoTouch.Foundation.NSObject dellegate, MonoTouch.ObjCRuntime.Selector dataPreparedSelector)
		{
			_delegateToCall = dellegate;
			_selectorToCall = dataPreparedSelector;

			bool isCameraAvailable = 
					UIImagePickerController.IsCameraDeviceAvailable (UIImagePickerControllerCameraDevice.Front) ||
					UIImagePickerController.IsCameraDeviceAvailable (UIImagePickerControllerCameraDevice.Rear);

			if (isCameraAvailable) {
				var ctl = new UIImagePickerController ();
				ctl.ImagePickerControllerDelegate = new ImagePickerDelegate(ctl, HandlePickedImage);
				ctl.SourceType = UIImagePickerControllerSourceType.Camera;
				ctl.CameraCaptureMode = UIImagePickerControllerCameraCaptureMode.Photo;
				_imagePicker = ctl;
				_viewController.PresentViewController(_imagePicker, true, null);
			}
		}

		public MonoTouch.Foundation.NSString GetLabelOfDataToSend ()
		{
			return new MonoTouch.Foundation.NSString (string.Format("Foto genomen om '{0}'", System.DateTime.Now.ToShortTimeString()));
		}

		public MonoTouch.Foundation.NSData GetDataToSend ()
		{
				return _imageData;
		}

		public bool StoreData (MonoTouch.Foundation.NSData data, MonoTouch.Foundation.NSObject andReplyToDelegate, MonoTouch.ObjCRuntime.Selector selector)
		{
			bool result = false;
			_delegateToCall = andReplyToDelegate;
			_selectorToCall = selector;

			if (data != null && data.Length != 0) {
				UIImage image = new UIImage (data);
				if (image != null) {
					NSNotificationCenter.DefaultCenter.PostNotificationName (BTConstants.BD_BLUETOOTH_NOTIFICATION_NEW_DATA_RECEIVED, null, GetDataAsDictionary (data));
					result = true;
				} 
			}
			_delegateToCall.PerformSelector (_selectorToCall, null, 0.0);
			return result;
		}

		public NSDictionary GetDataAsDictionary (NSData data)
		{
			NSDictionary dictionary = NSDictionary.FromObjectAndKey (data, new NSString(BTConstants.BD_BLUETOOTH_NEW_DATA_KEY));
			return dictionary;
		}

		#endregion
		void HandlePickedImage (UIImage image, NSDictionary dict)
		{
			_imageData = image.AsJPEG ();	//Scale (new System.Drawing.SizeF(320f, 240f)).AsJPEG ();
			_imagePicker = null;
			if (_delegateToCall != null && _selectorToCall != null && _delegateToCall.RespondsToSelector (_selectorToCall)) {
				_delegateToCall.PerformSelector (_selectorToCall, null, 0f);
			}
		}

		public class ImagePickerDelegate : UIImagePickerControllerDelegate
		{
			private Action<UIImage, NSDictionary> _callback;
			public ImagePickerDelegate(UIImagePickerController parent, Action<UIImage,NSDictionary> callback){
				_callback = callback;
			}
			public override void FinishedPickingImage (UIImagePickerController picker, UIImage image, NSDictionary editingInfo)
			{
				picker.DismissViewController(true, () => _callback(image, editingInfo));
			}
		}
	}
}

