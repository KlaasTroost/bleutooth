using System;

namespace BD.Bluetooth.Connection
{
	public class BTConstants
	{
		public const string BD_BLUETOOTH_GLOBAL_SESSION_ID = @"bd_bt_uid";

		public const string BD_BLUETOOTH_NOTIFICATION_DEVICE_AVAILABLE = @"bd_bt_notif_device_available";
		public const string BD_BLUETOOTH_NOTIFICATION_DEVICE_UNAVAILABLE = @"bd_bt_notif_device_unavailable";
		public const string BD_BLUETOOTH_NOTIFICATION_DEVICE_CONNECTED = @"bd_bt_notif_device_connected";
		public const string BD_BLUETOOTH_NOTIFICATION_DEVICE_CONNECTION_FAILED = @"bd_bt_notif_device_connection_failed";
		public const string BD_BLUETOOTH_NOTIFICATION_DEVICE_DISCONNECTED = @"bd_bt_notif_device_disconnected";
		public const string BD_BLUETOOTH_NOTIFICATION_NEW_DATA_RECEIVED = @"bd_bt_notif_new_data_received";

		public const string BD_BLUETOOTH_DEVICE_KEY = @"bd_bt_device";
		public const string BD_BLUETOOTH_NEW_DATA_KEY = @"bd_bt_new_data";

	}
}

