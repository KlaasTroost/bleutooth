/**
 * Deze klasse ontvangt de wijzigen mbt de status van de bluetooth devices.
 * 
 * 
 * Met behulp van NSNotificationCenter.DefaultCenter worden onderstaande notificaties 
 * verstuurd middels PostNotificationName() :
 * 
 * 		BTConstants.BD_BLUETOOTH_NOTIFICATION_DEVICE_CONNECTED			(geeft currentDevice mee)
 * 		BTConstants.BD_BLUETOOTH_NOTIFICATION_DEVICE_AVAILABLE 			(geeft currentDevice mee)
 * 		BTConstants.BD_BLUETOOTH_NOTIFICATION_DEVICE_UNAVAILABLE		(geeft currentDevice mee)
 * 		BTConstants.BD_BLUETOOTH_NOTIFICATION_DEVICE_UNCONNECTED		(geeft currentDevice mee)
 * 		BTConstants.BD_BLUETOOTH_NOTIFICATION_DEVICE_CONNECTION_FAILED	(geeft currentDevice mee)
 * 		BTConstants.BD_BLUETOOTH_NOTIFICATION_NEW_DATA_RECEIVED			(nieuwe data)
 * 
 * De extra informatie die meegestuurd wordt, en dus ook uitgelezen kan worden, wordt eerst in een
 * NSDictionary gestopt, hieronder zie je welke key bij welke data hoort:
 * 
 * 		BTConstants.BD_BLUETOOTH_DEVICE_KEY		currentDevice
 * 		BTConstants.BD_BLUETOOTH_NEW_DATA_KEY	nieuwe data (IDataprovider.GetDataToSend()).
 * 
 * De verstuurde of ontvangen data 
 **/

using System;
using MonoTouch.GameKit;
using System.Collections;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using BD.Bluetooth.Core;


namespace BD.Bluetooth.Connection
{
	public class BTSessionManager : GKSessionDelegate
	{
		BTDeviceManager _devicesManager;
		GKSession _gkSession;
		BTDataHandler _dataHandler;

		public BTSessionManager (BTDataHandler dataHandler, BTDeviceManager deviceManager)
		{
			_devicesManager = deviceManager;
			_dataHandler = dataHandler;

			_gkSession = new GKSession (BTConstants.BD_BLUETOOTH_GLOBAL_SESSION_ID, null, GKSessionMode.Peer);
			_gkSession.Delegate = this;
			_gkSession.ReceiveData += HandleReceiveData;
		}

		void HandleReceiveData (object sender, GKDataReceivedEventArgs e)
		{
			_dataHandler.ReceiveData (e.Data, e.PeerID, e.Session, null);
		}

		public void start() {
			InvokeOnMainThread (delegate {
				_gkSession.Available = true;
			});
		}
		public override void PeerChangedState (GKSession session, string peerID, GKPeerConnectionState state)
		{
#if DEBUG
			Console.WriteLine ("SessionManager.PeerChangedState(session: {0}, peerID: {1}, state: {2}", session.PeerID, peerID, state);
#endif
			BTDevice currentDevice = _devicesManager.DeviceWithID (peerID);

			// Instead of trying to respond to the event directly, it delegates the events.
			// The availability is checked by the main ViewController.
			// The connection is verified by each Device.
			switch (state) {
			case GKPeerConnectionState.Connected:
				if (currentDevice != null) {
					NSNotificationCenter.DefaultCenter.PostNotificationName(BTConstants.BD_BLUETOOTH_NOTIFICATION_DEVICE_CONNECTED, null, GetDeviceInfo(currentDevice));
				}
				break;
			case GKPeerConnectionState.Connecting:
			case GKPeerConnectionState.Available:
				if (currentDevice == null) {
					currentDevice = AddDevice(peerID);
					NSNotificationCenter.DefaultCenter.PostNotificationName(BTConstants.BD_BLUETOOTH_NOTIFICATION_DEVICE_AVAILABLE, null, GetDeviceInfo(currentDevice));
				}
				break;
			case GKPeerConnectionState.Unavailable:
				if (currentDevice != null) {
					RemoveDevice(currentDevice);
					NSNotificationCenter.DefaultCenter.PostNotificationName(BTConstants.BD_BLUETOOTH_NOTIFICATION_DEVICE_UNAVAILABLE, null, GetDeviceInfo(currentDevice));
					currentDevice = null;
				}
				break;
			case GKPeerConnectionState.Disconnected:
				if (currentDevice != null) {
					NSNotificationCenter.DefaultCenter.PostNotificationName(BTConstants.BD_BLUETOOTH_NOTIFICATION_DEVICE_DISCONNECTED, null, GetDeviceInfo(currentDevice));
				}
				break;
			}
		}
		public BTDevice AddDevice(string peerID){
#if DEBUG
			Console.WriteLine ("SessionManager.AddDevice(peerID: {0})", peerID);
#endif
			BTDevice device = new BTDevice (_gkSession, peerID);
			_devicesManager.AddDevice (device);
			return device;
		}
		public void RemoveDevice(BTDevice device){
#if DEBUG
			Console.WriteLine ("SessionManager.RemoveDevice(device: {0})", device.PeerID);
#endif
			_devicesManager.removeDevice (device);
		}
		public NSDictionary GetDeviceInfo(BTDevice device){
#if DEBUG
			Console.WriteLine ("SessionManager.GetDeviceInfo(device: {0})", device.PeerID);
#endif
			NSDictionary dictionary = NSDictionary.FromObjectAndKey (device, new NSString(BTConstants.BD_BLUETOOTH_DEVICE_KEY));
			return dictionary;
		}
		public override void PeerConnectionRequest (GKSession session, string peerID)
		{
			_gkSession.AcceptConnection(peerID, IntPtr.Zero);
		}

		public override void PeerConnectionFailed (GKSession session, string peerID, NSError error)
		{
			BTDevice currentDevice = _devicesManager.DeviceWithID(peerID);

			// Does the same thing as the didStateChange method. It tells a Device that the connection failed.
			if (currentDevice != null) {
				NSNotificationCenter.DefaultCenter.PostNotificationName(BTConstants.BD_BLUETOOTH_NOTIFICATION_DEVICE_CONNECTION_FAILED, null, GetDeviceInfo(currentDevice));
			}
		}

		public override void FailedWithError (GKSession session, NSError error)
		{
			UIAlertView errorView = new UIAlertView(
				NSBundle.MainBundle.LocalizedString(@"BD_BT_BLUETOOTH_ERROR_TITLE", @"Title for the error dialog."),
				NSBundle.MainBundle.LocalizedString(@"BD_BT_BLUETOOTH_ERROR", @"Wasn't able to make bluetooth available"),
				new ErrorViewDelegate(),
			    @"OK",
			    null);

			errorView.Show();
		}

		public class ErrorViewDelegate : UIAlertViewDelegate
		{
			public override void Clicked (UIAlertView alertview, int buttonIndex)
			{
//				alertview.DismissWithClickedButtonIndex (0, true);
			}
		}
	}
}

