using System;
using MonoTouch.GameKit;
using MonoTouch.Foundation;

namespace BD.Bluetooth.Connection
{
	public class BTDevice : NSObject
	{
		public const double CONNECTION_TIMEOUT = 10.0;

		GKSession _gkSession;

		string _peerID;
		string _deviceName;

		NSObject _delegateToCall;
		MonoTouch.ObjCRuntime.Selector _connectionWasStablished;
		MonoTouch.ObjCRuntime.Selector _connectionWasNotStablished;

		public BTDevice(GKSession session, string peerID){
			_gkSession = session;

			_peerID = peerID;
			_deviceName = session.DisplayNameForPeer (peerID);
		}
		
		public string PeerID { get { return _peerID; } }
		public string DeviceName { get { return _deviceName; } }

		public void ConnectAndReplyTo(NSObject dellegate, MonoTouch.ObjCRuntime.Selector cws, MonoTouch.ObjCRuntime.Selector cwns) {
#if DEBUG
			Console.WriteLine ("Device.ConnectAndReplyTo()");
#endif
			// We need to persist this info, because the call to connect is assynchronous.
			_delegateToCall = dellegate;
			_connectionWasStablished = cws;
			_connectionWasNotStablished = cwns;

			// The SessionManager will be responsible for sending the notification that will be caught here.
			NSNotificationCenter.DefaultCenter.AddObserver (this, 
			                                               new MonoTouch.ObjCRuntime.Selector ("TriggerConnectionSuccessfull"), 
			                                               BTConstants.BD_BLUETOOTH_NOTIFICATION_DEVICE_CONNECTED,
			                                               null);
			NSNotificationCenter.DefaultCenter.AddObserver (this,
			                                                new MonoTouch.ObjCRuntime.Selector("TriggerConnectionFailed"),
			                                                BTConstants.BD_BLUETOOTH_NOTIFICATION_DEVICE_CONNECTION_FAILED,
			                                                null);
			NSNotificationCenter.DefaultCenter.AddObserver (this,
			                                                new MonoTouch.ObjCRuntime.Selector("TriggerConnectionFailed"),
			                                                BTConstants.BD_BLUETOOTH_NOTIFICATION_DEVICE_UNAVAILABLE, 
			                                                null);

			_gkSession.Connect (PeerID, CONNECTION_TIMEOUT);
		}
		[Export("TriggerConnectionSuccessfull")]
		public void TriggerConnectionSuccessfull(NSNotification notification ){
#if DEBUG
			Console.WriteLine ("Device.TriggerConnectionSuccessfull(NSNotification: {0})", notification);
#endif
			BTDevice device = (BTDevice)notification.UserInfo.ObjectForKey(NSObject.FromObject(BTConstants.BD_BLUETOOTH_DEVICE_KEY));

			if (device != null && this.Equals(device) && _delegateToCall != null && _delegateToCall.RespondsToSelector(_connectionWasStablished) ){
				_delegateToCall.PerformSelector (_connectionWasStablished, null, 0);
				_delegateToCall = null;
				_connectionWasStablished = null;
				_connectionWasNotStablished = null;
			}
		}

		[Export("TriggerConnectionFailed")]
		public void TriggerConnectionFailed(NSNotification notification ) {
#if DEBUG
			Console.WriteLine ("Device.TriggerConnectionFailed(NSNotification: {0})", notification);
#endif
			BTDevice device = (BTDevice)notification.UserInfo.ObjectForKey(NSObject.FromObject(BTConstants.BD_BLUETOOTH_DEVICE_KEY));

			if (device != null && this.Equals(device) && _delegateToCall != null && _delegateToCall.RespondsToSelector(_connectionWasNotStablished)) {
				_delegateToCall.PerformSelector (_connectionWasNotStablished, null, 0);
				_delegateToCall = null;
				_connectionWasNotStablished = null;
				_connectionWasStablished = null;
			}
		}

		public void Disconnect() {
#if DEBUG
			Console.WriteLine ("Device.Disconnect()");

#endif
			_gkSession.DisconnectPeerFromAllPeers (PeerID);
		}
		public void CancelConnection() {
#if DEBUG
			Console.WriteLine ("Device.CancelConnection()");
#endif
			_gkSession.CancelConnect (PeerID);
		}

		public bool IsConnected () {
#if DEBUG
			Console.WriteLine ("Device.IsConnected()");
#endif
			bool found = false;
			string[] peers = _gkSession.PeersWithConnectionState (GKPeerConnectionState.Connected);

			foreach (string p in peers) {
				if (p.Equals(PeerID)) {
					found = true;
					break;
				}
			}
			return found;
		}

		public bool SendData(NSData data, out NSError error) {
#if DEBUG
			Console.WriteLine ("Device.SendData()");
#endif
			bool success = _gkSession.SendData (data, new string[] { _peerID }, GKSendDataMode.Reliable, out error);
#if DEBUG
			if (error != null && error.Code != 0) {
				Console.WriteLine ("BTDevice.SendData() - Fout opgetreden - error.Code: {0}", error.Code);
			} else {
				Console.WriteLine ("BTDevice.SendData() - Succesvol");
			}
#endif

			return success;
		}
		public override bool Equals (object obj)
		{
#if DEBUG
			Console.WriteLine ("Device.Equals()");
#endif
			return obj != null && (obj is BTDevice) && (((BTDevice)obj).PeerID.Equals(PeerID));

		}
		public override int GetHashCode ()
		{
#if DEBUG
			Console.WriteLine ("Device.GetHashCode()");
#endif
			return PeerID.GetHashCode ();
		}
	}
}

