using System;
using System.Collections.Generic;
using System.Collections;

namespace BD.Bluetooth.Connection
{
	public class BTDeviceManager : IComparer<BTDevice>
	{
		List<BTDevice> _devices;

		public BTDeviceManager ()
		{
			_devices = new List<BTDevice> ();
		}

		public List<BTDevice> SortedDevices { 
			get {
				return _devices;
			} 
		}

		public void AddDevice(BTDevice device) {
			_devices.Add (device);
			_devices.Sort (this);
		}

		public void removeDevice(BTDevice device) {
			if (device != null) {
				_devices.Remove (device);
			}
		}

		public BTDevice DeviceWithID(string peerID) {
			foreach (BTDevice d in _devices) {
				if (d.PeerID.Equals(peerID)) {
					return d;
				}
			}

			return null;
		}

		#region IComparer implementation

		public int Compare (BTDevice x, BTDevice y)
		{
			return String.CompareOrdinal (x.PeerID, y.PeerID);
		}

		#endregion
	}
}

