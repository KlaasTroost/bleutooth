using System;
using System.Drawing;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using MonoTouch.GameKit;
using System.Threading;
using System.IO;
using BD.Bluetooth.Connection;
using BD.Bluetooth.Core;
using BD.Bluetooth.Providers;

namespace BD.Bluetooth
{
	public partial class BluetoothViewController : UIViewController	//<UITableViewDelegate, UITableViewDataSource>
	{
		BTDeviceManager _devicesManager;
		BTDataHandler _dataHandler;
		BTSessionManager _sessionManager;

		public BluetoothViewController ()
			: base ("BluetoothViewController_iPad", null)
		{
		}
		public override void ViewDidLoad ()
		{
			_devicesManager = new BTDeviceManager ();
			_dataHandler = new BTDataHandler (CreateSpecificDataProvider(), _devicesManager);
			_sessionManager = new BTSessionManager (_dataHandler, _devicesManager);

			devicesTable.SeparatorColor = UIColor.Gray;
			devicesTable.Source = new DevicesTableViewSource (_devicesManager, _dataHandler);

			deviceNameLabel.Text = string.Format ("Naam: {0} - {1}", UIDevice.CurrentDevice.Name, UIDevice.CurrentDevice.IdentifierForVendor.AsString ());

			InvokeInBackground (delegate {
				// Notifications being called from the SessionManager when devices become available/unavailable
				NSNotificationCenter.DefaultCenter.AddObserver (this, new MonoTouch.ObjCRuntime.Selector ("DeviceAvailable"), BTConstants.BD_BLUETOOTH_NOTIFICATION_DEVICE_AVAILABLE, null);
				NSNotificationCenter.DefaultCenter.AddObserver (this, new MonoTouch.ObjCRuntime.Selector ("DeviceUnavailable"), BTConstants.BD_BLUETOOTH_NOTIFICATION_DEVICE_UNAVAILABLE, null);
				NSNotificationCenter.DefaultCenter.AddObserver (this, new MonoTouch.ObjCRuntime.Selector ("NewDataReceived"), BTConstants.BD_BLUETOOTH_NOTIFICATION_NEW_DATA_RECEIVED, null);

				_sessionManager.start ();
			});
		}
		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);

		}
		IBTDataProvider CreateSpecificDataProvider() {
			return new BTImageDataProvider (this);
		}

		[Export("DeviceAvailable")]
		void DeviceAvailable(NSNotification notification) {
#if DEBUG
			Console.WriteLine("DeviceAvailable");
#endif
			InvokeOnMainThread (delegate {
				devicesTable.ReloadData ();
			});
		}

		[Export("DeviceUnavailable")]
		void DeviceUnavailable(NSNotification notification) {
#if DEBUG
			Console.WriteLine("DeviceUnavailable");
#endif
			InvokeOnMainThread (delegate {
				devicesTable.ReloadData ();
			});
		}

		[Export("NewDataReceived")]
		void NewDataReceived(NSNotification notification) {
			InvokeOnMainThread (delegate {
#if DEBUG
				Console.WriteLine("NewDataReceived");
#endif
				NSDictionary dictionary = notification.UserInfo;
				if (dictionary.ContainsKey(NSObject.FromObject(BTConstants.BD_BLUETOOTH_NEW_DATA_KEY))) {
					NSData data = (NSData)dictionary.ObjectForKey (NSObject.FromObject(BTConstants.BD_BLUETOOTH_NEW_DATA_KEY));
					UIImage img = new UIImage (data);
					if (img != null) {
						imageView.Image = img;
					}
				}
			});
		}

		#region DevicesTableViewSource
		public class DevicesTableViewSource : UITableViewSource
		{
			BTDeviceManager _devicesManager;
			BTDataHandler _dataHandler;


			public DevicesTableViewSource(BTDeviceManager devMgr, BTDataHandler dataHandler) {
				_devicesManager = devMgr;
				_dataHandler = dataHandler;
			}
			public override int NumberOfSections (UITableView tableView)
			{
				return 1;
			}

			public override int RowsInSection (UITableView tableView, int section)
			{
#if DEBUG
				Console.WriteLine("RowsInSection {0}", _devicesManager.SortedDevices.Count);
#endif
				if (_devicesManager.SortedDevices.Count > 0)
					Console.WriteLine ("RowsInSection: {0} - {1}", _devicesManager.SortedDevices [0].DeviceName, tableView.Frame.Height);
				return _devicesManager.SortedDevices.Count;
			}

			public override void RowSelected (UITableView tableView, NSIndexPath indexPath)
			{
#if DEBUG
				Console.WriteLine("RowSelected - indexPath.Row: {0} - tableView.Height: {1}", indexPath.Row, tableView.Frame.Height);
#endif
				DeviceCell cell = (DeviceCell)tableView.CellAt (indexPath);
				BTDevice device = cell.Device;

				_dataHandler.SendToDevice (device);

				tableView.DeselectRow (indexPath, true);
			}

			public override UITableViewCell GetCell(UITableView tableView, NSIndexPath indexPath)
			{
#if DEBUG
				Console.WriteLine ("GetCell - indexPath.Row: {0}", indexPath.Row);
#endif
				const string CellIdentifier = @"DeviceCell";

				DeviceCell cell = tableView.DequeueReusableCell (CellIdentifier) as DeviceCell;
				if (cell == null) {
					cell = new DeviceCell (UITableViewCellStyle.Default, CellIdentifier);
				}
				BTDevice device = _devicesManager.SortedDevices [indexPath.Row];
				cell.SetDevice(device);

				return cell;
			}
		}
		#endregion
	}
	
	#region DeviceCell
	public class DeviceCell : UITableViewCell {
		BTDevice _device;
		public BTDevice Device {
			get { return _device; }
		}
		public DeviceCell(UITableViewCellStyle style, string identifier) : base( style, identifier)
		{
#if DEBUG
			Console.WriteLine("DeviceCell constructor - style: {0}, identifier: {1}", style, identifier);
#endif
			Frame = new RectangleF (0f, 0f, 480f, 48f);
		}

		public void SetDevice(BTDevice device) {
			_device = device;
			this.TextLabel.Text = string.Format ("{0} - {1}", _device.DeviceName ,_device.PeerID);
			this.TextLabel.TextColor = UIColor.Black;
			this.SelectionStyle = UITableViewCellSelectionStyle.Gray;
		}
	}
	#endregion
}

